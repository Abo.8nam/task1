﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace task1
{
    public class MyHub : Hub
    {
        public void Announce(string message, string type_alert)
        {
            Clients.All.Announce(message,type_alert);
        }
    }
}