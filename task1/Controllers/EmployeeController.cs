﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using task1.Models;

namespace task1.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetEmployees()
        {
            using (mycontext db = new mycontext())
            {
                var employees = db.Employees.OrderBy(e => e.Id).ToList();
                return Json(new { data = employees }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}