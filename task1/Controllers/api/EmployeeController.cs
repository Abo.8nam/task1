﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using task1.Models;

namespace task1.Controllers.api
{
    public class EmployeeController : ApiController
    {
        mycontext db = new mycontext();


        //Get Employees
        [HttpGet]
        public IHttpActionResult GetEmployees()
        {
            //var data = ;

            return Ok(db.Employees.ToList());
        }

        //Get Employees

        [HttpGet]
        [Route("api/Employees")]
        public IHttpActionResult GetEmployees2()
        {
            //var data = ;

            return Ok(new { data = db.Employees.ToList() });
        }

        //post Employee
        [HttpPost]
        public IHttpActionResult InsertEmployee(Employee employee)
        {
            if (!ModelState.IsValid)
                return BadRequest("Invalid Data");
            else
            {
                db.Employees.Add(new Employee()
                {
                    username = employee.username,
                    email = employee.email,
                    salary = employee.salary,
                    city = employee.city
                });

                db.SaveChanges();
                return Ok(new { msg = "Saved..." });
            }
        }


        [HttpPut]
        public IHttpActionResult UpdateEmployee([FromBody]Employee employee)
        {
            Employee e = db.Employees.FirstOrDefault(ee => ee.Id == employee.Id);

            if (e == null)
                return NotFound();
            else
            {
                e.username = employee.username;
                e.email = employee.email;
                e.salary = employee.salary;
                e.city = employee.city;

                db.SaveChanges();

                return Ok(new { msg = "Edited..." });
            }
        }

        [HttpDelete]
        public IHttpActionResult Delete([FromBody]int id)
        {
            //var ex = db.Exams.Select(ex_qu => ex_qu.Questions.Select(ex_quu => ex_quu.ID == id)) as Question;
            Employee q = db.Employees.FirstOrDefault(qq => qq.Id == id);


            db.Employees.Remove(q);
            db.SaveChanges();

            return Ok(new { msg = "Removed..." });
        }
    }
}
