﻿//$(document).ready(function () {
//    alert("Worked Well...");
//});


$.connection.hub.start()
    .done(function () {
        console.log("IT Worked ...!")

        //Button Delete
        $("#btnDelete").click(function () {

            //debugger;
            //Sending Data To All
            //debugger;
            var id = $("#txt_delete_id").val();

            $.ajax({
                type: "Delete",
                url: '/api/Employee',
                data: JSON.stringify(id),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function () {
                    //console.log('success', emp);
                    // alert("Data has been added successfully.");
                    debugger;
                    //$(function () {

                        $('#modalDelete').modal('toggle');
                    //});


                    //var alert1 = `<div id="alertt_${id}" class="alert alert-success" role="alert">
                    //                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    //                        <span aria-hidden="true">
                    //                            &times;
                    //                        </span>
                    //                    </button>
                    //                    <strong>
                    //                        Deleted
                    //                    </strong>
                    //                        Item Success!
                    //                    </div> `
                    //$("#alert1").append(alert1);

                    //window.setTimeout(function () {
                    //    $(`#alertt_${id}`).fadeTo(0, 200).slideUp(200, function () {
                    //        $(this).remove();
                    //    });
                    //}, 5000);

                    //LoadData();
                    $.connection.myHub.server.announce("You Deleted Successfully", "danger");

                },
                error: function () {
                    alert("Error while inserting data");

                }
            });
            return false;
        });



        // Button Save
        $("#btnSave").click(function () {
            debugger;

            var emp = {};
            emp.username = $("#Emp_username").val();
            emp.email = $("#Emp_email").val();

            //function ValidateEmail(mail) {
            //    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(Form.email.value)) {
            //        return (true)
            //    }
            //    alert("You have entered an invalid email address!")
            //    return (false)
            //}

            emp.salary = $("#Emp_salary").val();
            emp.city = $("#Emp_city").val();
            $.ajax({
                type: "POST",
                url: '/api/Employee',
                data: JSON.stringify(emp),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function () {
                    console.log('success', emp);
                    //LoadData();

                    //Or Use LoadDate()   >> hint The Two Function Work Well
                    //function loaddatatable() {

                    //    //debugger;
                    //    $("#tblEmployee tbody tr").remove();
                    //    $(".mydatatable").DataTable({
                    //        "ajax": {
                    //            "url": "/Employee/GetEmployees",
                    //            "type": "GET",
                    //            "datatype": "json"
                    //        },
                    //        "columns": [
                    //            { "data": "Id", "autoWidth": true },
                    //            { "data": "username", "autoWidth": true },
                    //            { "data": "email", "autoWidth": true },
                    //            { "data": "salary", "autoWidth": true },
                    //            { "data": "city", "autoWidth": true },
                    //            {
                    //                "data": "Id", "render": function (data) {
                    //                    return '<a href="#" class="updatemodal btn btn-success" id="aupdate" data-toggle="modal" data-target="#modalRegister" > Update</a>';
                    //                }
                    //            },
                    //            {
                    //                "data": "Id", "render": function (data) {
                    //                    return '<a href="#" class="deletemodal btn btn-danger" id="adelete" data-toggle="modal" data-target="#modalDelete" >Delete</a>';
                    //                }
                    //            }
                    //        ]
                    //    })

                    //}



                    $.connection.myHub.server.announce("You Inserted Successfully", "primary");
                },
                error: function () {
                    alert("Error while inserting data");
                }
            });

            return false;
        });


        // Button Update
        $("#btnUpdate").click(function () {

            var emp = {};
            emp.id = $("#Emp_Id_Update").val();
            emp.username = $("#Emp_username_Update").val();
            emp.email = $("#Emp_email_Update").val();
            emp.salary = $("#Emp_salary_Update").val();
            emp.city = $("#Emp_city_Update").val();
            $.ajax({
                type: "PUT",
                url: '/api/Employee',
                data: JSON.stringify(emp),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function () {
                    //console.log('success', emp);
                    // alert("Data has been added successfully.");
                    $(function () {
                        $('#modalRegister').modal('toggle');
                    });

                    //Fun Bellow Causes Of Dublicate
                    //LoadData();
                    $.connection.myHub.server.announce("You Updated Successfully", "success");

                },
                error: function () {
                    alert("Error while inserting data");
                }
            });
            return false;
        });

        //debugger;
        //LoadData();

        //$.connection.myHub.server.announce("Connected");
    })
    .fail(function () {
        alert("Error !!!!!!!")
    })



//When Recive
$.connection.myHub.client.announce = function (message, type_alert) {

    //loading Data With All Clients
    LoadData();
    
    
    //alert For All Clients with (8) Seconds
    var aclass = `alert alert-${type_alert}`;
    var alert1 = `<div id="alertt" class="${aclass}" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">
                                                &times;
                                            </span>
                                        </button>
                                        <strong>
                                            ${message}
                                        </strong>
                                        </div> `
    $("#alert1").append(alert1);

    window.setTimeout(function () {
        $(`#alertt`).fadeTo(0, 200).slideUp(200, function () {
            $(this).remove();
        });
    }, 8000);


}