﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace task1.Models
{
    public class Employee
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string username { get; set; }
        [Required]
        public string email  { get; set; }
        public float? salary { get; set; }
        [Required]
        public string city { get; set; }
    }
}