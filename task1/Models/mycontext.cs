﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace task1.Models
{
    public class mycontext:DbContext
    {
        public mycontext() : base("MyCon")
        {

        }
        public virtual DbSet<Employee> Employees { get; set; }
    }
}